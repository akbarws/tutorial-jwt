package com.akbar.jwt.service;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.akbar.jwt.entity.User;
import com.akbar.jwt.repository.UserRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> userFromDb = userRepository.findByUsername(username);
		if (userFromDb.isPresent()) {
			User user = userFromDb.get();
			return new org.springframework.security.core.userdetails.User(
					user.getUsername(), 
					user.getPassword(), 
					Arrays.stream(user.getRole().getName().split(",")).map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}

}

package com.akbar.jwt.model;

import java.io.Serializable;

public class JwtResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6437939944284827897L;
	
	private final String jwtToken;

	public String getJwtToken() {
		return jwtToken;
	}

	public JwtResponse(String jwtToken) {
		super();
		this.jwtToken = jwtToken;
	}
	

}
